/* main.c */
/*
    This file is part of bit-guess.
    Copyright (C) 2018 bg nerilex (bg@nerilex.org)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
#include <stdio.h>
#include <stdlib.h>
#include <inttypes.h>

#include "bit-guess.h"

index_t f0(const bit_t tape[], index_t max) {
    /* fill in your function here */
    return 0;
}

double test(select_index_func *f, size_t length, unsigned repeat) {
    unsigned ret = 0;
    size_t i;
    unsigned t;
    index_t x, y;
    bit_t a[length], b[length];
    for (t = 0; t < repeat; ++t) {
        for (i = 0; i < length; ++i) {
            a[i] = rand() % 2;
            b[i] = rand() % 2;
        }
        x = f(a, repeat);
        y = f(b, repeat);
        ret += (a[y] == b[x]);
    }
    return ((double)ret / repeat);
}

int main(void) {
    size_t len = 100, fi;
    unsigned repeat = 1000000;
    double val;
    select_index_func *f[] = { f0 };

    srand(1337);
    for (fi = 0; fi < sizeof(f) / sizeof(f[0]); fi++) {
        val = test(f[fi], len, repeat);
        printf("[f = %2zd; l = %8zd; r = %8u] prob = %f\n", fi, len, repeat, val);
    }
    return 0;
}
